const items = [
    {
        id: 1,
        name: "Samsung Galaxy s21",
        price: 840,
        category: 'Smartphone',
        description: "Smartphone with 512gb, 6gb ram, 120mp camera, gorilla glas 6"
    },
    {
        id: 2,
        name: "Xiaomi mi 11",
        price: 640,
        category: 'Smartphone',
        description: "Smartphone with 128gb, 4gb ram, 100mp camera"
    },
    {
        id: 3,
        name: "IPhone 12 pro max",
        price: 1140,
        category: 'Smartphone',
        description: "Smartphone with 256gb, 3gb ram, 3 camera, api ios cip 15"
    },
    {
        id: 4,
        name: "huawei mate p40 pro",
        price: 740,
        category: 'Smartphone',
        description: "Smartphone with 128gb, 6gb ram, 120mp camera super amoled"
    },
    {
        id: 5,
        name: "Samsung Galaxy z flip 3",
        price: 1840,
        category: 'Smartphone',
        description: "Smartphone with 128gb, 6gb ram, 60mp camera,flip phone,beautifull look"
    },
]

export default items